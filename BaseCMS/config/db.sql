/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50519
Source Host           : localhost:3306
Source Database       : weixincms

Target Server Type    : MYSQL
Target Server Version : 50519
File Encoding         : 65001

Date: 2016-05-13 16:33:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `smsverifycode`
-- ----------------------------
DROP TABLE IF EXISTS `smsverifycode`;
CREATE TABLE `smsverifycode` (
  `id` varchar(255) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `expierDate` datetime DEFAULT NULL,
  `phoneNum` varchar(11) DEFAULT NULL,
  `verifyCode` varchar(255) DEFAULT NULL,
  `verifyCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of smsverifycode
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_action`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_action`;
CREATE TABLE `t_cms_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `parenT_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKB7EE77DDBD9F6DBF` (`parenT_id`),
  CONSTRAINT `FKB7EE77DDBD9F6DBF` FOREIGN KEY (`parenT_id`) REFERENCES `t_cms_action` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_cms_action
-- ----------------------------
INSERT INTO `t_cms_action` VALUES ('1', 'CMS系统', '/', '', null);
INSERT INTO `t_cms_action` VALUES ('2', '安全管理', '', '', '1');
INSERT INTO `t_cms_action` VALUES ('3', '用户管理', 'admin', '', '2');
INSERT INTO `t_cms_action` VALUES ('4', '查询用户', 'admin/list', '', '3');
INSERT INTO `t_cms_action` VALUES ('5', '新增用户', 'admin/add', '', '3');
INSERT INTO `t_cms_action` VALUES ('6', '修改用户', 'admin/modify', '', '3');
INSERT INTO `t_cms_action` VALUES ('7', '删除用户', 'admin/delete', '', '3');
INSERT INTO `t_cms_action` VALUES ('8', '解冻用户', 'admin/unlock', '', '3');
INSERT INTO `t_cms_action` VALUES ('9', '重置密码', 'admin/resetPwd', '', '3');
INSERT INTO `t_cms_action` VALUES ('10', '角色管理', 'role', '', '2');
INSERT INTO `t_cms_action` VALUES ('11', '查询角色', 'role/list', '', '10');
INSERT INTO `t_cms_action` VALUES ('12', '新增角色', 'role/add', '', '10');
INSERT INTO `t_cms_action` VALUES ('13', '修改角色', 'role/modify', '', '10');
INSERT INTO `t_cms_action` VALUES ('14', '删除角色', 'role/delete', '', '10');
INSERT INTO `t_cms_action` VALUES ('15', '资源管理', 'action', '', '2');
INSERT INTO `t_cms_action` VALUES ('16', '查询资源', 'action/list', '', '15');
INSERT INTO `t_cms_action` VALUES ('17', '新增资源', 'action/add', '', '15');
INSERT INTO `t_cms_action` VALUES ('18', '修改资源', 'action/modify', '', '15');
INSERT INTO `t_cms_action` VALUES ('19', '删除资源', 'action/delete', '', '15');
INSERT INTO `t_cms_action` VALUES ('24', '数据字典', 'datadict', '', '2');
INSERT INTO `t_cms_action` VALUES ('25', '查询字典', 'datadict/list', '', '24');
INSERT INTO `t_cms_action` VALUES ('26', '新增字典', 'datadict/add', '', '24');
INSERT INTO `t_cms_action` VALUES ('27', '修改字典', 'datadict/modify', '', '24');
INSERT INTO `t_cms_action` VALUES ('28', '删除字典', 'datadict/delete', '', '24');
INSERT INTO `t_cms_action` VALUES ('29', '数据操作', '', '', '1');
INSERT INTO `t_cms_action` VALUES ('30', '会员管理', 'user', '', '29');
INSERT INTO `t_cms_action` VALUES ('31', '会员列表', 'user/list', '', '30');
INSERT INTO `t_cms_action` VALUES ('32', '会员新增', 'user/add', '', '30');
INSERT INTO `t_cms_action` VALUES ('33', '会员修改', 'user/modify', '', '30');
INSERT INTO `t_cms_action` VALUES ('34', '会员删除', 'user/delete', '', '30');
INSERT INTO `t_cms_action` VALUES ('35', '会员详情', 'user/view', '', '30');
INSERT INTO `t_cms_action` VALUES ('36', '会员密码重置', 'user/resetPwd', '', '30');
INSERT INTO `t_cms_action` VALUES ('37', '栏目管理', 'catgory', '', '29');
INSERT INTO `t_cms_action` VALUES ('38', '栏目列表', 'catgory/list', '', '37');
INSERT INTO `t_cms_action` VALUES ('39', '栏目新增', 'catgory/add', '', '37');
INSERT INTO `t_cms_action` VALUES ('40', '栏目修改', 'catgory/modify', '', '37');
INSERT INTO `t_cms_action` VALUES ('41', '栏目删除', 'catgory/delete', '', '37');
INSERT INTO `t_cms_action` VALUES ('42', '栏目详情', 'catgory/view', '', '37');
INSERT INTO `t_cms_action` VALUES ('43', '文章管理', 'article', '', '29');
INSERT INTO `t_cms_action` VALUES ('44', '文章列表', 'article/list', '', '43');
INSERT INTO `t_cms_action` VALUES ('45', '文章新增', 'article/add', '', '43');
INSERT INTO `t_cms_action` VALUES ('46', '文章修改', 'article/modify', '', '43');
INSERT INTO `t_cms_action` VALUES ('47', '文章删除', 'article/delete', '', '43');
INSERT INTO `t_cms_action` VALUES ('48', '文章详情', 'article/view', '', '43');
INSERT INTO `t_cms_action` VALUES ('49', '公众号管理', 'account', '', '29');
INSERT INTO `t_cms_action` VALUES ('50', '公众号列表', 'account/list', '', '49');
INSERT INTO `t_cms_action` VALUES ('51', '公众号新增', 'account/add', '', '49');
INSERT INTO `t_cms_action` VALUES ('52', '公众号修改', 'account/modify', '', '49');
INSERT INTO `t_cms_action` VALUES ('53', '公众号删除', 'account/del', '', '49');
INSERT INTO `t_cms_action` VALUES ('54', '公众号详情', 'account/view', '', '49');
INSERT INTO `t_cms_action` VALUES ('55', '图文消息管理', 'msgnews', '', '29');
INSERT INTO `t_cms_action` VALUES ('56', '图文消息列表', 'msgnews/list', '', '55');
INSERT INTO `t_cms_action` VALUES ('57', '图文消息新增', 'msgnews/add', '', '55');
INSERT INTO `t_cms_action` VALUES ('58', '图文消息修改', 'msgnews/modify', '', '55');
INSERT INTO `t_cms_action` VALUES ('59', '图文消息删除', 'msgnews/del', '', '55');
INSERT INTO `t_cms_action` VALUES ('60', '图文消息详情', 'msgnews/view', '', '55');
INSERT INTO `t_cms_action` VALUES ('61', '微信账号菜单', 'accountMenu', '', '29');
INSERT INTO `t_cms_action` VALUES ('62', '微信账号菜单列表', 'accountMenu/list', '', '61');
INSERT INTO `t_cms_action` VALUES ('63', '微信账号菜单新增', 'accountMenu/add', '', '61');
INSERT INTO `t_cms_action` VALUES ('64', '微信账号菜单修改', 'accountMenu/modify', '', '61');
INSERT INTO `t_cms_action` VALUES ('65', '微信账号菜单删除', 'accountMenu/del', '', '61');
INSERT INTO `t_cms_action` VALUES ('66', '微信账号菜单详情', 'accountMenu/view', '', '61');

-- ----------------------------
-- Table structure for `t_cms_action_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_action_role`;
CREATE TABLE `t_cms_action_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9C041B984E0A7CB3` (`action_id`),
  KEY `FK9C041B989DD2AEF3` (`role_id`),
  CONSTRAINT `FK9C041B984E0A7CB3` FOREIGN KEY (`action_id`) REFERENCES `t_cms_action` (`id`),
  CONSTRAINT `FK9C041B989DD2AEF3` FOREIGN KEY (`role_id`) REFERENCES `t_cms_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_cms_action_role
-- ----------------------------
INSERT INTO `t_cms_action_role` VALUES ('1', '1', '5');
INSERT INTO `t_cms_action_role` VALUES ('2', '2', '5');
INSERT INTO `t_cms_action_role` VALUES ('3', '3', '5');
INSERT INTO `t_cms_action_role` VALUES ('4', '4', '5');
INSERT INTO `t_cms_action_role` VALUES ('5', '5', '5');
INSERT INTO `t_cms_action_role` VALUES ('6', '6', '5');
INSERT INTO `t_cms_action_role` VALUES ('7', '7', '5');
INSERT INTO `t_cms_action_role` VALUES ('8', '8', '5');
INSERT INTO `t_cms_action_role` VALUES ('9', '9', '5');
INSERT INTO `t_cms_action_role` VALUES ('10', '10', '5');
INSERT INTO `t_cms_action_role` VALUES ('11', '11', '5');
INSERT INTO `t_cms_action_role` VALUES ('12', '12', '5');
INSERT INTO `t_cms_action_role` VALUES ('13', '13', '5');
INSERT INTO `t_cms_action_role` VALUES ('14', '14', '5');
INSERT INTO `t_cms_action_role` VALUES ('15', '15', '5');
INSERT INTO `t_cms_action_role` VALUES ('16', '16', '5');
INSERT INTO `t_cms_action_role` VALUES ('17', '17', '5');
INSERT INTO `t_cms_action_role` VALUES ('18', '18', '5');
INSERT INTO `t_cms_action_role` VALUES ('19', '19', '5');
INSERT INTO `t_cms_action_role` VALUES ('24', '24', '5');
INSERT INTO `t_cms_action_role` VALUES ('25', '25', '5');
INSERT INTO `t_cms_action_role` VALUES ('26', '26', '5');
INSERT INTO `t_cms_action_role` VALUES ('27', '27', '5');
INSERT INTO `t_cms_action_role` VALUES ('28', '28', '5');
INSERT INTO `t_cms_action_role` VALUES ('29', '29', '5');
INSERT INTO `t_cms_action_role` VALUES ('30', '30', '5');
INSERT INTO `t_cms_action_role` VALUES ('31', '31', '5');
INSERT INTO `t_cms_action_role` VALUES ('32', '32', '5');
INSERT INTO `t_cms_action_role` VALUES ('33', '33', '5');
INSERT INTO `t_cms_action_role` VALUES ('34', '34', '5');
INSERT INTO `t_cms_action_role` VALUES ('35', '35', '5');
INSERT INTO `t_cms_action_role` VALUES ('36', '36', '5');
INSERT INTO `t_cms_action_role` VALUES ('37', '37', '5');
INSERT INTO `t_cms_action_role` VALUES ('38', '38', '5');
INSERT INTO `t_cms_action_role` VALUES ('39', '39', '5');
INSERT INTO `t_cms_action_role` VALUES ('40', '40', '5');
INSERT INTO `t_cms_action_role` VALUES ('41', '41', '5');
INSERT INTO `t_cms_action_role` VALUES ('42', '42', '5');
INSERT INTO `t_cms_action_role` VALUES ('43', '43', '5');
INSERT INTO `t_cms_action_role` VALUES ('44', '44', '5');
INSERT INTO `t_cms_action_role` VALUES ('45', '45', '5');
INSERT INTO `t_cms_action_role` VALUES ('46', '46', '5');
INSERT INTO `t_cms_action_role` VALUES ('47', '47', '5');
INSERT INTO `t_cms_action_role` VALUES ('48', '48', '5');
INSERT INTO `t_cms_action_role` VALUES ('49', '49', '5');
INSERT INTO `t_cms_action_role` VALUES ('50', '50', '5');
INSERT INTO `t_cms_action_role` VALUES ('51', '51', '5');
INSERT INTO `t_cms_action_role` VALUES ('52', '52', '5');
INSERT INTO `t_cms_action_role` VALUES ('53', '53', '5');
INSERT INTO `t_cms_action_role` VALUES ('54', '54', '5');
INSERT INTO `t_cms_action_role` VALUES ('55', '55', '5');
INSERT INTO `t_cms_action_role` VALUES ('56', '56', '5');
INSERT INTO `t_cms_action_role` VALUES ('57', '57', '5');
INSERT INTO `t_cms_action_role` VALUES ('58', '58', '5');
INSERT INTO `t_cms_action_role` VALUES ('59', '59', '5');
INSERT INTO `t_cms_action_role` VALUES ('60', '60', '5');
INSERT INTO `t_cms_action_role` VALUES ('61', '61', '5');
INSERT INTO `t_cms_action_role` VALUES ('62', '62', '5');
INSERT INTO `t_cms_action_role` VALUES ('63', '63', '5');
INSERT INTO `t_cms_action_role` VALUES ('64', '64', '5');
INSERT INTO `t_cms_action_role` VALUES ('65', '65', '5');
INSERT INTO `t_cms_action_role` VALUES ('66', '66', '5');

-- ----------------------------
-- Table structure for `t_cms_admin`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_admin`;
CREATE TABLE `t_cms_admin` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `lastlogintime` datetime DEFAULT NULL,
  `lastloginaddr` varchar(15) DEFAULT NULL,
  `realname` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `preLoginAddr` varchar(255) DEFAULT NULL,
  `preLoginTime` datetime DEFAULT NULL,
  `lastModifyPsdDate` datetime DEFAULT NULL,
  `loginFailureCount` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_cms_admin
-- ----------------------------
INSERT INTO `t_cms_admin` VALUES ('admin', '0192023a7bbd73250516f069df18b500', '1', '2016-05-13 16:29:56', '127.0.0.1', '系统默认账户', '系统', '127.0.0.1', '2016-05-13 16:19:00', '2014-12-30 13:43:32', '0', '0');
INSERT INTO `t_cms_admin` VALUES ('test', '0192023a7bbd73250516f069df18b500', '1', '2016-05-03 16:56:02', '127.0.0.1', '张鹏', '支付平台', '0:0:0:0:0:0:0:1', '2016-12-30 13:57:32', '2016-12-30 13:43:32', '0', '0');

-- ----------------------------
-- Table structure for `t_cms_admin_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_admin_role`;
CREATE TABLE `t_cms_admin_role` (
  `username` varchar(255) NOT NULL,
  `authority` int(11) NOT NULL,
  UNIQUE KEY `ix_auth_username` (`username`,`authority`),
  KEY `fk_authorities_authority` (`authority`),
  CONSTRAINT `fk_authorities_authority` FOREIGN KEY (`authority`) REFERENCES `t_cms_role` (`id`),
  CONSTRAINT `fk_authorities_users` FOREIGN KEY (`username`) REFERENCES `t_cms_admin` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_cms_admin_role
-- ----------------------------
INSERT INTO `t_cms_admin_role` VALUES ('admin', '5');
INSERT INTO `t_cms_admin_role` VALUES ('test', '6');

-- ----------------------------
-- Table structure for `t_cms_article`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_article`;
CREATE TABLE `t_cms_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catgoryId` int(11) DEFAULT NULL,
  `content` text,
  `modifyDate` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `showOrder` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_cms_article
-- ----------------------------
INSERT INTO `t_cms_article` VALUES ('2', null, '<p>3333dd发射点发烧饭</p>', '2016-05-11 10:30:00', '333fff', null, '1', '333');
INSERT INTO `t_cms_article` VALUES ('3', null, '<p><img src=\"http://7xkeg5.com1.z0.glb.clouddn.com/67002757-be6b-4457-9a42-ff98bc24ebab.jpg\" title=\"a5\" alt=\"a5.jpg\"/>&nbsp; &nbsp; &nbsp;<img src=\"http://7xkeg5.com1.z0.glb.clouddn.com/71f856b3-ffab-414d-ad97-edb15e0adad2.jpg\" title=\"a7\" alt=\"a7.jpg\"/>发生大幅</p>', '2016-05-11 11:04:22', '44', null, '1', '44');
INSERT INTO `t_cms_article` VALUES ('4', null, '<p>dddddddfsdfsdf发射点发但是</p>', '2016-05-11 11:04:13', '111', null, '1', '11');
INSERT INTO `t_cms_article` VALUES ('6', null, '<fieldset data-id=\"2185\" style=\"border: 0px none; padding: 0px; box-sizing: border-box; margin: 0px; font-size: 14px; font-family: 微软雅黑;\" class=\"135editor\"><section style=\"color: inherit; font-size: 16px; padding: 5px 10px 0px 35px; margin-left: 27px; border-left-width: 2px; border-left-style: dotted; border-left-color: rgb(228, 228, 228); box-sizing: border-box;\"><section class=\"autonum\" style=\"width: 32px; height: 32px; margin-left: -53px; margin-top: 23px; color: rgb(202, 251, 215); text-align: center; line-height: 32px; border-radius: 16px; box-sizing: border-box; padding: 0px; background: rgb(14, 184, 58);\" data-original-title=\"\" title=\"\" data-width=\"32px\">1</section><section class=\"135brush\" style=\"margin-top: -30px; padding-bottom: 10px; color: inherit; box-sizing: border-box;\"><p style=\"clear: both; line-height: 1.5em; font-size: 14px; color: inherit; box-sizing: border-box; padding: 0px; margin: 0px;\"><span style=\"color: inherit; font-size: 16px; box-sizing: border-box; padding: 0px; margin: 0px;\"><strong class=\"135title\" style=\"color: inherit; box-sizing: border-box; padding: 0px; margin: 0px;\">如何进入【135编辑器】？</strong></span></p><p style=\"clear: both; line-height: 1.5em; font-size: 14px; color: inherit; box-sizing: border-box; padding: 0px; margin: 0px;\">网页搜索“135编辑器”，点击第一条搜索结果即可进入编辑器页面</p></section></section></fieldset><p style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"><br style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"/></p><fieldset data-id=\"14\" style=\"border: 0px none; box-sizing: border-box; padding: 0px; margin: 0px; font-size: 14px; font-family: 微软雅黑;\" class=\"135editor\"><blockquote style=\"border-width: 1px 1px 1px 5px; border-style: solid; border-color: rgb(238, 238, 238) rgb(238, 238, 238) rgb(238, 238, 238) rgb(159, 136, 127); border-radius: 3px; padding: 10px; margin: 10px 0px; box-sizing: border-box;\"><h4 class=\"135brush\" style=\"color: rgb(159, 136, 127); font-weight: bold; font-size: 18px; margin: 5px 0px; border-color: rgb(159, 136, 127); box-sizing: border-box; padding: 0px;\">标题文字</h4><section class=\"135brush\" data-style=\"color:inherit;font-size:14px;\" style=\"color: inherit; font-size: 14px; box-sizing: border-box; padding: 0px; margin: 0px;\"><p style=\"box-sizing: border-box; padding: 0px; margin: 0px;\">内容描述.</p></section></blockquote></fieldset><p style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"><br style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"/></p><fieldset data-id=\"33\" style=\"border: 0px none; box-sizing: border-box; padding: 0px; margin: 0px; font-size: 14px; font-family: 微软雅黑;\" class=\"135editor\"><h2 style=\"margin: 8px 0px 0px; padding: 0px; font-weight: bold; font-size: 16px; line-height: 28px; max-width: 100%; color: rgb(0, 112, 192); min-height: 32px; border-bottom-width: 2px; border-bottom-style: solid; border-bottom-color: rgb(0, 112, 192); text-align: justify; box-sizing: border-box;\"><span class=\"autonum\" placeholder=\"1\" style=\"border-radius: 80% 100% 90% 20%; color: rgb(255, 255, 255); display: block; float: left; line-height: 20px; margin: 0px 8px 0px 0px; max-width: 100%; padding: 4px 10px; word-wrap: break-word !important; box-sizing: border-box; background-color: rgb(0, 112, 192);\" data-original-title=\"\" title=\"\">1</span><strong class=\"135brush\" data-brushtype=\"text\" style=\"box-sizing: border-box; padding: 0px; margin: 0px;\">第一标题</strong></h2></fieldset><p style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"><br style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"/></p><fieldset data-id=\"126\" style=\"border: 0px none; box-sizing: border-box; padding: 0px; margin: 0px; font-size: 14px; font-family: 微软雅黑;\" class=\"135editor\"><fieldset style=\"border: 0px; margin: 5px 0px; box-sizing: border-box; padding: 0px;\"><section style=\"height: 1em; box-sizing: border-box; padding: 0px; margin: 0px;\"><section style=\"height: 100%; width: 1.5em; float: left; border-top-width: 0.4em; border-top-style: solid; border-color: rgb(249, 110, 87); border-left-width: 0.4em; border-left-style: solid; box-sizing: border-box; padding: 0px; margin: 0px;\" data-width=\"1.5em\"></section><section style=\"height: 100%; width: 1.5em; float: right; border-top-width: 0.4em; border-top-style: solid; border-color: rgb(249, 110, 87); border-right-width: 0.4em; border-right-style: solid; box-sizing: border-box; padding: 0px; margin: 0px;\" data-width=\"1.5em\"></section><section style=\"display: inline-block; color: transparent; clear: both; box-sizing: border-box; padding: 0px; margin: 0px;\"></section></section><section style=\"margin: -0.8em 0.1em -0.8em 0.2em; padding: 0.8em; border: 1px solid rgb(249, 110, 87); border-radius: 0.3em; box-sizing: border-box;\"><section placeholder=\"四角宽边框的样式\" style=\"color: rgb(51, 51, 51); font-size: 1em; line-height: 1.4; word-break: break-all; word-wrap: break-word; box-sizing: border-box; padding: 0px; margin: 0px;\" class=\"135brush\">四角宽边框的样式</section></section><section style=\"height: 1em; box-sizing: border-box; padding: 0px; margin: 0px;\"><section style=\"height: 100%; width: 1.5em; float: left; border-bottom-width: 0.4em; border-bottom-style: solid; border-color: rgb(249, 110, 87); border-left-width: 0.4em; border-left-style: solid; box-sizing: border-box; padding: 0px; margin: 0px;\" data-width=\"1.5em\"></section><section style=\"height: 100%; width: 1.5em; float: right; border-bottom-width: 0.4em; border-bottom-style: solid; border-color: rgb(249, 110, 87); border-right-width: 0.4em; border-right-style: solid; box-sizing: border-box; padding: 0px; margin: 0px;\" data-width=\"1.5em\"></section></section></fieldset></fieldset><p style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"><br style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"/></p><fieldset data-id=\"39\" style=\"border: 0px none; box-sizing: border-box; padding: 0px; margin: 0px; font-size: 14px; font-family: 微软雅黑;\" class=\"135editor\"><section style=\"max-width: 100%; margin: 0.8em 0px 0.5em; overflow: hidden; box-sizing: border-box; padding: 0px;\"><section class=\"135brush\" data-brushtype=\"text\" placeholder=\"请输入标题\" style=\"height: 36px; display: inline-block; color: rgb(255, 255, 255); font-size: 16px; font-weight: bold; padding: 0px 10px; line-height: 36px; float: left; vertical-align: top; box-sizing: border-box; margin: 0px; background-color: rgb(249, 110, 87);\">请输入标题</section><section style=\"display: inline-block; height: 36px; vertical-align: top; border-left-width: 9px; border-left-style: solid; border-left-color: rgb(249, 110, 87); box-sizing: border-box; border-top-width: 18px !important; border-top-style: solid !important; border-top-color: transparent !important; border-bottom-width: 18px !important; border-bottom-style: solid !important; border-bottom-color: transparent !important; padding: 0px; margin: 0px;\"></section></section></fieldset><p style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"><br style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"/></p><fieldset data-id=\"5\" style=\"border: 0px none; box-sizing: border-box; padding: 0px; margin: 0px; font-size: 14px; font-family: 微软雅黑;\" class=\"135editor\"><blockquote class=\"135brush\" style=\"white-space: normal; font-size: 14px; line-height: 25px; margin: 2px 0px; padding: 10px; border: 2px dashed rgb(222, 220, 222); max-width: 100%; box-sizing: border-box;\"><p placeholder=\"虚线框内容，作为摘要或段落内容。\" style=\"box-sizing: border-box; padding: 0px; margin: 0px;\">虚线框内容，作为摘要或段落内容。</p></blockquote></fieldset><p style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"><br style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"/></p><fieldset data-id=\"29735\" style=\"border: 0px none; box-sizing: border-box; padding: 0px; margin: 0px; font-size: 14px; font-family: 微软雅黑;\" class=\"135editor\"><section style=\"width: 92px; margin-bottom: 0px; box-sizing: border-box; padding: 0px;\" data-width=\"92px\"><p style=\"text-align: center; color: inherit; line-height: 2em; box-sizing: border-box; padding: 0px; margin: 0px;\"><span style=\"border-color: rgb(255, 129, 36); color: rgb(255, 129, 36); font-size: 16px; box-sizing: border-box; padding: 0px; margin: 0px;\"><strong style=\"border-color: rgb(255, 129, 36); color: inherit; box-sizing: border-box; padding: 0px; margin: 0px;\">135编辑器</strong></span></p></section><fieldset style=\"margin-top: 0px; padding: 0px 5px; line-height: 10px; color: inherit; border: 1px solid rgb(255, 129, 36); box-sizing: border-box;\"><section style=\"padding: 0px; font-size: 16px; color: inherit; height: 8px; margin: -8px 0px 0px 140px; width: 50%; box-sizing: border-box; background-color: rgb(254, 254, 254);\" data-width=\"50%\"><section style=\"width: 8px; height: 8px; border-radius: 100%; line-height: 1; box-sizing: border-box; font-size: 18px; text-decoration: inherit; border-color: rgb(255, 129, 36); display: inline-block; margin: 0px; color: inherit; padding: 0px; background-color: rgb(255, 129, 36);\" data-width=\"8px\"></section></section><section class=\"135brush\" data-style=\"text-indent: 2em;\" style=\"padding: 0px; line-height: 2em; color: rgb(62, 62, 62); font-size: 14px; margin: 15px; box-sizing: border-box;\"><p style=\"text-indent: 2em; color: inherit; box-sizing: border-box; padding: 0px; margin: 0px;\">135编辑器是一个在线图文编辑工具，大量节省您排版的时间，让工作更轻松高效。</p></section><section style=\"padding: 0px; font-size: 16px; color: inherit; text-align: right; height: 10px; margin: 0px 0px -4px 25px; width: 65%; box-sizing: border-box; background-color: rgb(254, 254, 254);\" data-width=\"65%\"><section style=\"margin: 0px auto 1px; border-radius: 100%; line-height: 1; box-sizing: border-box; text-decoration: inherit; border-color: rgb(255, 129, 36); display: inline-block; height: 8px; width: 8px; color: inherit; padding: 0px; background-color: rgb(255, 129, 36);\" data-width=\"8px\"></section></section></fieldset></fieldset><p style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"><br style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"/></p><fieldset data-id=\"391\" style=\"border: 0px none; box-sizing: border-box; padding: 0px; margin: 0px; font-size: 14px; font-family: 微软雅黑;\" class=\"135editor\"><fieldset style=\"margin: 2em 1em 1em; padding: 0px; border: 0px rgb(255, 179, 167); border-image-source: none; max-width: 100%; box-sizing: border-box; color: rgb(62, 62, 62); font-size: 16px; line-height: 25px; word-wrap: break-word !important;\"><section style=\"max-width: 100%; box-sizing: border-box; line-height: 1.4; margin-left: -0.5em; word-wrap: break-word !important; padding: 0px;\"><section style=\"max-width: 100%; box-sizing: border-box; border-color: rgb(0, 0, 0); padding: 3px 8px; border-radius: 4px; color: rgb(167, 23, 0); font-size: 1em; display: inline-block; transform: rotate(-10deg); transform-origin: 0% 100% 0px; word-wrap: break-word !important; margin: 0px; background-color: rgb(255, 179, 167);\"><span class=\"135brush\" data-brushtype=\"text\" style=\"color: rgb(255, 255, 255); box-sizing: border-box; padding: 0px; margin: 0px;\">135编辑器</span></section></section><section class=\"135brush\" data-style=\"line-height:24px;color:rgb(89, 89, 89); font-size:14px\" style=\"max-width: 100%; box-sizing: border-box; padding: 22px 16px 16px; border: 1px solid rgb(255, 179, 167);color: rgb(0, 0, 0); font-size: 14px;margin-top: -24px;\"><p style=\"line-height: 24px; box-sizing: border-box; padding: 0px; margin: 0px;\"><span style=\"color: rgb(89, 89, 89); font-size: 14px; box-sizing: border-box; padding: 0px; margin: 0px;\">135编辑器提供非常好用的微信图文编辑器。可以随心所欲的变换颜色调整格式，更有神奇的自动配色方案。</span></p></section></fieldset></fieldset><p style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"><br style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"/></p><p style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"><br style=\"box-sizing: border-box; padding: 0px; margin: 0px;\"/></p>', '2016-05-11 14:59:10', '222', null, '1', '222');
INSERT INTO `t_cms_article` VALUES ('7', null, '<p>555555555</p>', '2016-05-11 15:04:31', '55555555', null, '1', '4555555555');

-- ----------------------------
-- Table structure for `t_cms_bus`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_bus`;
CREATE TABLE `t_cms_bus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `modifyDate` datetime DEFAULT NULL,
  `createUser` varchar(255) DEFAULT NULL,
  `fromAddr` varchar(255) DEFAULT NULL,
  `goDate` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `seats` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `toAddr` varchar(255) DEFAULT NULL,
  `createDate_end` datetime DEFAULT NULL,
  `createDate_start` datetime DEFAULT NULL,
  `modifyDate_end` datetime DEFAULT NULL,
  `modifyDate_start` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_cms_bus
-- ----------------------------
INSERT INTO `t_cms_bus` VALUES ('1', null, null, null, '成都', '2016-02-02 20:13', '30', '对对对', '49', '1', '眉山', null, null, null, null);

-- ----------------------------
-- Table structure for `t_cms_catgory`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_catgory`;
CREATE TABLE `t_cms_catgory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `modifyDate` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `showOrder` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_cms_catgory
-- ----------------------------
INSERT INTO `t_cms_catgory` VALUES ('1', '1', null, '1', '2', '0');
INSERT INTO `t_cms_catgory` VALUES ('3', '2222', '2016-04-08 17:44:56', '淡淡的', '2', '0');

-- ----------------------------
-- Table structure for `t_cms_datadict`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_datadict`;
CREATE TABLE `t_cms_datadict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `nameCN` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_cms_datadict
-- ----------------------------
INSERT INTO `t_cms_datadict` VALUES ('1', '正常', 'userStatus', '0', '用户状态');
INSERT INTO `t_cms_datadict` VALUES ('2', '注销', 'userStatus', '1', '用户状态');
INSERT INTO `t_cms_datadict` VALUES ('3', '启用', 'catgoryStatus', '0', '栏目状态');
INSERT INTO `t_cms_datadict` VALUES ('4', '关闭', 'catgoryStatus', '1', '栏目状态');
INSERT INTO `t_cms_datadict` VALUES ('6', '正常', 'articleStatus', '1', '文章状态');
INSERT INTO `t_cms_datadict` VALUES ('7', '关闭', 'articleStatus', '2', '文章状态');
INSERT INTO `t_cms_datadict` VALUES ('8', '推荐', 'articleStatus', '3', '文章状态');
INSERT INTO `t_cms_datadict` VALUES ('9', '置顶', 'articleStatus', '4', '文章状态');
INSERT INTO `t_cms_datadict` VALUES ('10', '链接消息', 'menuEventType', 'view', '菜单事件类型');
INSERT INTO `t_cms_datadict` VALUES ('11', '事件消息', 'menuEventType', 'click', '菜单事件类型');
-- ----------------------------
-- Table structure for `t_cms_operatelog`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_operatelog`;
CREATE TABLE `t_cms_operatelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opCnMethodName` varchar(255) DEFAULT NULL,
  `opDate` datetime DEFAULT NULL,
  `opMethodName` varchar(255) DEFAULT NULL,
  `opModule` varchar(255) DEFAULT NULL,
  `opPerson` varchar(32) DEFAULT NULL,
  `opRemark` varchar(255) DEFAULT NULL,
  `opState` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_cms_operatelog
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_order`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_order`;
CREATE TABLE `t_cms_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `modifyDate` datetime DEFAULT NULL,
  `busId` int(11) DEFAULT NULL,
  `fromAddr` varchar(255) DEFAULT NULL,
  `goDate` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `seats` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `toAddr` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  `createDate_end` datetime DEFAULT NULL,
  `createDate_start` datetime DEFAULT NULL,
  `modifyDate_end` datetime DEFAULT NULL,
  `modifyDate_start` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_cms_order
-- ----------------------------
INSERT INTO `t_cms_order` VALUES ('4', null, null, '1', '成都', '2016-02-02 20:13', '30', '49', '1', '眉山', '15928165825', null, null, null, null);
INSERT INTO `t_cms_order` VALUES ('5', null, null, '1', '成都', '2016-02-02 20:13', '30', '49', '1', '眉山', '15928165825', null, null, null, null);
INSERT INTO `t_cms_order` VALUES ('6', null, null, '1', '成都', '2016-02-02 20:13', '30', '49', '3', '眉山', '15928165825', null, null, null, null);
INSERT INTO `t_cms_order` VALUES ('7', null, null, '1', '成都', '2016-02-02 20:13', '30', '49', '2', '眉山', '15928165825', null, null, null, null);

-- ----------------------------
-- Table structure for `t_cms_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_role`;
CREATE TABLE `t_cms_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authority` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_cms_role
-- ----------------------------
INSERT INTO `t_cms_role` VALUES ('5', 'ROLE_ADMIN', '管理员', '2014-07-22 00:00:00');
INSERT INTO `t_cms_role` VALUES ('6', 'ROLE_ALL', '所有业务功能', '2014-07-22 00:00:00');

-- ----------------------------
-- Table structure for `t_cms_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_user`;
CREATE TABLE `t_cms_user` (
  `userName` varchar(255) NOT NULL,
  `carImg` varchar(255) DEFAULT NULL,
  `carType` int(11) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `drivingLicense` varchar(255) DEFAULT NULL,
  `faceUrl` varchar(255) DEFAULT NULL,
  `niceName` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `roadLicense` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `taxiLicense` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_cms_user
-- ----------------------------
INSERT INTO `t_cms_user` VALUES ('15898889858', null, null, '2016-04-05 16:47:39', null, 'http://7xkeg5.com1.z0.glb.clouddn.com/201604051647371204860057.gif', '休息休息', '311bc68c5e9024971512af01feae6216', null, '0', null);
INSERT INTO `t_cms_user` VALUES ('15928165825', null, null, '2016-04-05 16:47:11', null, 'http://7xkeg5.com1.z0.glb.clouddn.com/20160405164706569234245.gif', '淡淡的', '311bc68c5e9024971512af01feae6216', null, '0', null);
INSERT INTO `t_cms_user` VALUES ('15928165826', 'http://7xprgj.com1.z0.glb.clouddn.com/apicloud/9b2fac9771444a9ab3e56c19984903c6.jpg', '1', '2016-04-04 14:44:21', 'http://7xprgj.com1.z0.glb.clouddn.com/apicloud/d0b0bb7e24153e7d87127f07d624f988.jpg', null, '11', '311bc68c5e9024971512af01feae6216', 'http://7xprgj.com1.z0.glb.clouddn.com/apicloud/f254c90add34d9c7aa04ee21386499e6.jpg', '0', 'http://7xprgj.com1.z0.glb.clouddn.com/apicloud/f6c639b7d762e4c55d245636a32083b2.jpg');
INSERT INTO `t_cms_user` VALUES ('15988558588', null, null, '2016-04-05 16:31:50', null, 'http://7xkeg5.com1.z0.glb.clouddn.com/201604051627021400389321.gif', '55555666', null, null, '1', null);
INSERT INTO `t_cms_user` VALUES ('1658585858', null, null, '2016-04-01 17:06:45', null, 'http://7xkeg5.com1.z0.glb.clouddn.com/20160401170643990978143.gif', '李四', '06ed107f5e945913ed39fd43f9cfcf7ce4cf8205', null, '0', null);
INSERT INTO `t_cms_user` VALUES ('333', null, null, '2016-04-08 17:00:01', null, null, '333', '06ed107f5e945913ed39fd43f9cfcf7ce4cf8205', null, '0', null);
INSERT INTO `t_cms_user` VALUES ('333444', null, null, '2016-05-06 11:20:26', null, null, '333444', '06ed107f5e945913ed39fd43f9cfcf7ce4cf8205', null, '0', null);

-- ----------------------------
-- Table structure for `t_weixin_account`
-- ----------------------------
DROP TABLE IF EXISTS `t_weixin_account`;
CREATE TABLE `t_weixin_account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `modifyDate` datetime DEFAULT NULL,
  `account` varchar(255) NOT NULL,
  `appid` varchar(255) DEFAULT NULL,
  `appsecret` varchar(255) DEFAULT NULL,
  `dubbleMa` varchar(255) DEFAULT NULL,
  `msgcount` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_weixin_account
-- ----------------------------
INSERT INTO `t_weixin_account` VALUES ('2', '2016-05-11 15:16:44', '2016-05-11 15:16:44', '111', '111', '111', null, null, '111', '222', '<p>1222222</p>');
INSERT INTO `t_weixin_account` VALUES ('3', '2016-05-13 16:23:25', '2016-05-13 16:23:25', '222', '222', '222', null, null, '222', '2222', '<p>2222</p>');

-- ----------------------------
-- Table structure for `t_weixin_menu`
-- ----------------------------
DROP TABLE IF EXISTS `t_weixin_menu`;
CREATE TABLE `t_weixin_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `modifyDate` datetime DEFAULT NULL,
  `eventType` varchar(255) DEFAULT NULL,
  `inputCode` varchar(255) DEFAULT NULL,
  `msgId` varchar(255) DEFAULT NULL,
  `mtype` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `parentName` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `wxAccount` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_weixin_menu
-- ----------------------------
INSERT INTO `t_weixin_menu` VALUES ('1', '2016-05-13 15:06:25', '2016-05-13 15:06:25', 'click', 'ceshi', null, null, '测试', null, '多多', '1', '1', '33', 'wx5668558555');

-- ----------------------------
-- Table structure for `t_weixin_msgnews`
-- ----------------------------
DROP TABLE IF EXISTS `t_weixin_msgnews`;
CREATE TABLE `t_weixin_msgnews` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `modifyDate` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `brief` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `enable` int(11) DEFAULT NULL,
  `favourcount` int(11) DEFAULT NULL,
  `fromurl` varchar(255) DEFAULT NULL,
  `inputcode` varchar(255) DEFAULT NULL,
  `msgtype` varchar(255) DEFAULT NULL,
  `picdir` varchar(255) DEFAULT NULL,
  `picpath` varchar(255) DEFAULT NULL,
  `readcount` int(11) DEFAULT NULL,
  `rule` varchar(255) DEFAULT NULL,
  `showpic` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `wxAccount` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_weixin_msgnews
-- ----------------------------
INSERT INTO `t_weixin_msgnews` VALUES ('2', '2016-05-10 14:08:39', '2016-05-10 14:08:39', 'hello', '你好', '中国', null, null, null, '11', null, null, 'http://7xkeg5.com1.z0.glb.clouddn.com/001f3438-f37c-45e9-ab27-7341da2fb0fb.jpg', null, null, null, '你好中国', null, null);

-- ----------------------------
-- Table structure for `t_weixin_msgtext`
-- ----------------------------
DROP TABLE IF EXISTS `t_weixin_msgtext`;
CREATE TABLE `t_weixin_msgtext` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `modifyDate` datetime DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `enable` int(11) DEFAULT NULL,
  `favourcount` int(11) DEFAULT NULL,
  `inputcode` varchar(255) DEFAULT NULL,
  `msgtype` varchar(255) DEFAULT NULL,
  `readcount` int(11) DEFAULT NULL,
  `rule` varchar(255) DEFAULT NULL,
  `wxAccount` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_weixin_msgtext
-- ----------------------------
