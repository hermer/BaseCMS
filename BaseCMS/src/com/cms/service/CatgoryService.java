package com.cms.service;

import java.util.List;

import com.cms.entity.Catgory;

public interface CatgoryService {

	public Catgory loadById(Integer id);

	public List<Catgory> list(String wxAccount,String name,String status, String desc, Integer from, Integer to);

	public Long getCount(String wxAccount,String name, String status,String desc);

	public Catgory add(Catgory catgory);

	public boolean modify(Catgory catgory);

	public boolean delete(Integer id);
}