package com.cms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;

import com.cms.entity.Bus;
import com.cms.service.BusService;
import com.cms.support.Result;

@Service
public class BusServiceImpl extends BaseServiceImpl implements BusService {

	public Result updateStatus(Integer orderId, Integer status) {
		try {
			Bus bus = baseDao.get(Bus.class, orderId);
			bus.setStatus(status);
			baseDao.saveOrUpdate(bus);
			return new Result();
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, e.getMessage());
		}
	}

	@Override
	public List<Bus> list(Bus entity, Integer pageSize, Integer pageNum) {
		if (pageNum == null) {
			pageNum = 1;
		}
		if (pageSize == null) {
			pageSize = 20;
		}
		// 查询结果
		List<Object> any = new ArrayList<Object>();
		any.add(Transformers.aliasToBean(Bus.class));

		if (StringUtils.isNotBlank(entity.getFromAddr())) {
			any.add(Restrictions.eq("fromAddr", entity.getFromAddr().trim()));
		}
		if (StringUtils.isNotBlank(entity.getToAddr())) {
			any.add(Restrictions.eq("toAddr", entity.getToAddr().trim()));
		}
		if (entity.getStatus() != null) {
			any.add(Restrictions.eq("status", entity.getStatus()));
		}
		if (entity.getCreateUser() != null) {
			any.add(Restrictions.eq("createUser", entity.getCreateUser()));
		}
		any.add(Order.desc("createDate"));
		any.add(Projections.property("id").as("id"));
		any.add(Projections.property("fromAddr").as("fromAddr"));
		any.add(Projections.property("toAddr").as("toAddr"));
		any.add(Projections.property("price").as("price"));
		any.add(Projections.property("createDate").as("createDate"));
		any.add(Projections.property("createUser").as("createUser"));
		any.add(Projections.property("goDate").as("goDate"));
		any.add(Projections.property("seats").as("seats"));
		any.add(Projections.property("status").as("status"));
		any.add(Projections.property("remark").as("remark"));

		List<Bus> list = baseDao.findByAny(Bus.class, (pageNum - 1) * pageSize,
				pageSize, any.toArray());
		return list;
	}
}
