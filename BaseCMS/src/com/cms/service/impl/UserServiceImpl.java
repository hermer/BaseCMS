package com.cms.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cms.dto.UserReqDto;
import com.cms.dto.UserRespDto;
import com.cms.entity.User;
import com.cms.service.UserService;
import com.cms.support.Result;

@Service
public class UserServiceImpl extends BaseServiceImpl implements UserService {

	@Transactional
	@Override
	public Result add(User user) {
		try {
			baseDao.save(user);
		} catch (Exception e) {
			return new Result(false, e.getLocalizedMessage());
		}
		return new Result();
	}

	@Transactional
	@Override
	public Result update(User user) {
		try {
			baseDao.saveOrUpdate(user);
		} catch (Exception e) {
			return new Result(false, e.getLocalizedMessage());
		}
		return new Result();
	}

	@Transactional
	@Override
	public Result delete(String id) {
		try {
			baseDao.delete(this.get(id));
		} catch (Exception e) {
			return new Result(false, e.getLocalizedMessage());
		}
		return new Result();
	}

	@Transactional
	@Override
	public User get(String id) {
		User user;
		try {
			user = baseDao.get(User.class, id);
		} catch (Exception e) {
			return null;
		}
		return user;
	}

	@Transactional
	@Override
	public UserRespDto list(UserReqDto user, Integer from, Integer length) {
		UserRespDto userDto = new UserRespDto();
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// 查询结果
		List<Object> any = new ArrayList<Object>();
		any.add(Transformers.aliasToBean(User.class));
		if (StringUtils.isNotBlank(user.getUserName())) {
			any.add(Restrictions.eq("userName", user.getUserName().trim()));
		}
		if (user.getStatus() != null) {
			any.add(Restrictions.eq("status", user.getStatus()));
		}
		try {
			if (user.getCreateDateStart() != null) {
				any.add(Restrictions.gt("createDate",
						sf.parse(user.getCreateDateStart())));
			}
			if (user.getCreateDateEnd() != null) {
				any.add(Restrictions.le("createDate",
						sf.parse(user.getCreateDateEnd())));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (StringUtils.isNotBlank(user.getNiceName())) {
			any.add(Restrictions.like("niceName", user.getNiceName().trim(),
					MatchMode.ANYWHERE));
		}
		any.add(Projections.property("userName").as("userName"));
		any.add(Projections.property("niceName").as("niceName"));
		any.add(Projections.property("faceUrl").as("faceUrl"));
		any.add(Projections.property("status").as("status"));
		any.add(Projections.property("drivingLicense").as("drivingLicense"));
		any.add(Projections.property("taxiLicense").as("taxiLicense"));
		any.add(Projections.property("roadLicense").as("roadLicense"));
		any.add(Projections.property("carImg").as("carImg"));
		any.add(Projections.property("carType").as("carType"));
		any.add(Projections.property("createDate").as("createDate"));
		// 查询记录
		List<User> users = baseDao.findByAny(User.class, from, length,
				any.toArray());
		// 查询总记录数
		// 查询结果
		List<Criterion> anyCount = new ArrayList<Criterion>();
		// 查询条件
		if (StringUtils.isNotBlank(user.getUserName())) {
			anyCount.add(Restrictions.eq("userName", user.getUserName().trim()));
		}
		if (user.getStatus() != null) {
			anyCount.add(Restrictions.eq("status", user.getStatus()));
		}
		try {
			if (user.getCreateDateStart() != null) {
				any.add(Restrictions.gt("createDate",
						sf.parse(user.getCreateDateStart())));
			}
			if (user.getCreateDateEnd() != null) {
				any.add(Restrictions.le("createDate",
						sf.parse(user.getCreateDateEnd())));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (StringUtils.isNotBlank(user.getNiceName())) {
			anyCount.add(Restrictions.like("niceName", user.getNiceName()
					.trim(), MatchMode.ANYWHERE));
		}
		Long totalCount = baseDao.countByAny(User.class,
				anyCount.toArray(new Criterion[anyCount.size()]));
		userDto.setUsers(users);
		userDto.setTotalCount(totalCount);
		return userDto;
	}

	@Transactional
	@Override
	public List<User> getUsers(String userStr) {
		List<User> users = baseDao.sqlQuery(User.class,
				"select * from T_CMS_USER where userName in (" + userStr + ")");
		return users;
	}

}
