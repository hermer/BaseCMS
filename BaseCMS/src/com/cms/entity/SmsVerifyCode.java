package com.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "smsverifycode")
public class SmsVerifyCode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;// 请求标识//唯一标识不允许重复
	@Column(length = 11)
	private String phoneNum;// 手机号
	@Column
	private int verifyCount;// 验证次数
	@Column
	private String verifyCode;// 验证码
	@Column
	private Date expierDate;// 失效时间
	@Column
	private Date createDate;// 创建时间

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public int getVerifyCount() {
		return verifyCount;
	}

	public void setVerifyCount(int verifyCount) {
		this.verifyCount = verifyCount;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	public Date getExpierDate() {
		return expierDate;
	}

	public void setExpierDate(Date expierDate) {
		this.expierDate = expierDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
