package com.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "t_cms_admin")
public class Admin implements Serializable, UserDetails {
	private static final long serialVersionUID = 1L;

	private String username;
	private String password;
	private String passwordrep;
	private String realname;
	private String department;
	private boolean enabled;
	private Date lastLoginTime;
	private String lastLoginAddr;
	private Date preLoginTime;// 上次登录时间
	private String preLoginAddr;// 上次登录ip
	private Integer loginFailureCount;// 登录失败次数
	private Date lastModifyPsdDate;// 上次修改密码时间
	// 状态 ０：正常，１：冻结，２：注销
	private Integer status;
	private Set<Role> authorities;

	@Id
	@NotBlank(message = "请输入账号")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@NotBlank(message = "请输入密码")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Transient
	public String getPasswordrep() {
		return passwordrep;
	}

	public void setPasswordrep(String passwordrep) {
		this.passwordrep = passwordrep;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLastLoginAddr() {
		return lastLoginAddr;
	}

	public void setLastLoginAddr(String lastLoginAddr) {
		this.lastLoginAddr = lastLoginAddr;
	}

	@NotEmpty(message = "请至少选择一个角色")
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "t_cms_admin_role", joinColumns = { @JoinColumn(name = "username") }, inverseJoinColumns = { @JoinColumn(name = "authority") })
	public Set<Role> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Role> roles) {
		this.authorities = roles;
	}

	@Transient
	public boolean isAccountNonExpired() {
		return enabled;
	}

	@Transient
	public boolean isAccountNonLocked() {
		return enabled;
	}

	@Transient
	public boolean isCredentialsNonExpired() {
		return enabled;
	}

	public String getPreLoginAddr() {
		return preLoginAddr;
	}

	public void setPreLoginAddr(String preLoginAddr) {
		this.preLoginAddr = preLoginAddr;
	}

	public Date getPreLoginTime() {
		return preLoginTime;
	}

	public void setPreLoginTime(Date preLoginTime) {
		this.preLoginTime = preLoginTime;
	}

	public Integer getLoginFailureCount() {
		return loginFailureCount;
	}

	public void setLoginFailureCount(Integer loginFailureCount) {
		this.loginFailureCount = loginFailureCount;
	}

	public Date getLastModifyPsdDate() {
		return lastModifyPsdDate;
	}

	public void setLastModifyPsdDate(Date lastModifyPsdDate) {
		this.lastModifyPsdDate = lastModifyPsdDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	@Transient
	public String getStatusName() {
		String statusName = "正常";
		if (status != null) {
			if (status == 1) {
				statusName = "冻结";
			}
			if (status == 2) {
				statusName = "注销";
			}
		}
		return statusName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Admin [username=" + username + ", password=" + password
				+ ", passwordrep=" + passwordrep + ", realname=" + realname
				+ ", department=" + department + ", enabled=" + enabled
				+ ", lastLoginTime=" + lastLoginTime + ", lastLoginAddr="
				+ lastLoginAddr + ", preLoginTime=" + preLoginTime
				+ ", preLoginAddr=" + preLoginAddr + ", loginFailureCount="
				+ loginFailureCount + ", lastModifyPsdDate="
				+ lastModifyPsdDate + ", status=" + status + ", authorities="
				+ authorities + "]";
	}
	
}
