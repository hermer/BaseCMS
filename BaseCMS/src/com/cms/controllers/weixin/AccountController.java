package com.cms.controllers.weixin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cms.controllers.BaseController;
import com.cms.entity.weixin.Account;

/**
 * @ClassName: AccountController
 * @Description: TODO(微信账号管理)
 * @author zhangp
 * @date 2016年5月3日 上午10:04:08
 * 
 */
@Controller
@RequestMapping(value = "account")
public class AccountController extends BaseController<Account> {

	@Override
	protected String getPrefix() {
		return "account";
	}

	@Override
	protected Class<?> getClazz() {
		return Account.class;
	}

}
