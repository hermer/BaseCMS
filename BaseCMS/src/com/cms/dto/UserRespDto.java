package com.cms.dto;

import java.util.List;

import com.cms.entity.User;

public class UserRespDto {
	private List<User> users;
	private Long totalCount;

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}


}
