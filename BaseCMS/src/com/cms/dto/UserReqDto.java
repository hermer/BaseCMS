package com.cms.dto;

import java.io.Serializable;

public class UserReqDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private String userName;
	private String niceName;
	// 状态 ０：正常，1注销
	private Integer status;
	private String createDateStart;
	private String createDateEnd;


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getNiceName() {
		return niceName;
	}

	public void setNiceName(String niceName) {
		this.niceName = niceName;
	}

	/**
	 * @return the createDateStart
	 */
	public String getCreateDateStart() {
		return createDateStart;
	}

	/**
	 * @param createDateStart the createDateStart to set
	 */
	public void setCreateDateStart(String createDateStart) {
		this.createDateStart = createDateStart;
	}

	/**
	 * @return the createDateEnd
	 */
	public String getCreateDateEnd() {
		return createDateEnd;
	}

	/**
	 * @param createDateEnd the createDateEnd to set
	 */
	public void setCreateDateEnd(String createDateEnd) {
		this.createDateEnd = createDateEnd;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserReqDto [userName=" + userName + ", niceName=" + niceName
				+ ", status=" + status + ", createDateStart=" + createDateStart
				+ ", createDateEnd=" + createDateEnd + "]";
	}



}
