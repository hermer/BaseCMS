<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
<style>
body {
	background: #F5F5F5;
}

.form-container {
	margin-top: 40px;
}

.head-img {
	text-align: center;
}

.content {
	height: 140px;
	background: #fff;
	margin-top: 30px;
	padding-top: 20px;
}

.content .title-name {
	text-align: center;
}

border {
	border-right: 1px solid #EDF2F7;
	height: 100px;
}

.cont {
	font-size: 20px;
	text-align: center;
	margin-top: 25px;
}
</style>
</head>
<body>
	<div class="form-container">
		<div class="head-img">
			<img src="<c:url value="/images/touxiang.png"/>" />
			<div style="font-size: 20px; margin-top: 10px;">${thisUser.realname }</div>
			<div style="font-size: 16px; margin-top: 10px;">用户名：${thisUser.username }</div>
		</div>
		<div>
			<div class="content col-md-8 col-md-offset-2">
				<div class="col-md-4 border">
					<div class="title-name">部门</div>
					<div class="cont">${thisUser.department }</div>
				</div>
				<div class="col-md-4 border">
					<div class="title-name">上次登录时间</div>
					<div class="cont">
						<fmt:formatDate value="${thisUser.preLoginTime}"
							pattern="yyyy-MM-dd HH:mm:ss" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="title-name">当前登录IP</div>
					<div class="cont">${thisUser.preLoginAddr}</div>
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>