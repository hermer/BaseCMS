<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>手机QQ</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/success.css"/>" />
<style>
	
</style>
</head>

<body>
	<div class="outDiv">
		<div class="container con-container">	
			<img src="<c:url value="/images/portal/proCase/phoneqq.png"/>" class="img"/>
			<div class="content-one">深度定制NFC支付，银行IC卡闪付，无需绑卡，便捷支付快人一步。具有闪付（Quick Pass）字样的银联IC卡，是银联最新推出的产品之一，该产品具有非接触式支付的特性，同时具有小额快速支付的特征。在日常生活中，用户使用具备“闪付”功能的银联IC卡，在支持“闪付”的非接触式支付终端上，只需轻轻一挥，即可快速完成支付。
			</div>
			<div class="content-one">在日常生活中，用户使用具备“闪付”功能的银联IC卡，在支持“闪付”的非接触式支付终端上，只需轻轻一挥，即可快速完成支付。闪付功能已被广泛覆盖于日常小额支付商户，包括超市、便利店、药房、菜市场、停车场、加油站等零售场所和公共服务区域。目前，银联闪付卡发卡量已经突破2亿张。
			</div>
			<div class="content-one">手机QQ则是移动支付领域首个支持银联IC卡“闪付”的主流应用。使用手机QQ轻松刷IC闪付卡的前提是要求手机支持NFC功能。支持了QQ会员充值续费、QQ特权充值续费，充Q币，充书币等功能。
			</div>
		</div>
	</div>
</body>
</html>
