<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>微信支付-成功案例</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/success.css"/>" />
</head>
<body>
	<div class="navbar-pay">
		<div class="nav_width">
			<div class="container">
				<a class="navbar-brand" href="<c:url value="/"/>"><img
					src="<c:url value="/images/portal/one/ikkpay_1.png"/>" class="logo" /></a>
				<ul>
					<li><a href="<c:url value="/portal"/>" >主页</a></li>
					<li><a href="<c:url value="/portal/product"/>">产品展示</a></li>
					<li><a href="<c:url value="/portal/proCase"/>" class="active">成功案例</a></li>
					<li><a href="<c:url value="/portal/security"/>">安全保障</a></li>
					<li><a href="<c:url value="/portal/support"/>">技术支持</a></li>
					<li><a href="<c:url value="/portal/about"/>">关于我们</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="success" id="success">
		<div class="logo">
			<div class="container">
				<div class="titleBig active">
					<a href="#" target="_parent" class="phoneQQ" onclick="return false">
						<img src="<c:url value="/images/portal/proCase/qqN.png"/>"
						alt="qq" />
						<div class="title">手机QQ</div>
					</a>
				</div>
				<div class="titleBig">
					<a href="#" target="_parent" class="CUP" onclick="return false">
						<img src="<c:url value="/images/portal/proCase/cup.png"/>"
						alt="cup" />
						<div class="title">当乐网</div>
					</a>
				</div>
				<div class="titleBig">
					<a href="#" target="_parent" class="Financial"
						onclick="return false"> <img
						src="<c:url value="/images/portal/proCase/tsm.png"/>" alt="tsm" />
						<div class="title">蜀彩宝</div>
					</a>
				</div>
				<div class="titleBig">
					<a href="#" target="_parent" class="yaan" onclick="return false">
						<img src="<c:url value="/images/portal/proCase/yaan.png"/>"
						alt="yaan" />
						<div class="title">农行跨行通</div>
					</a>
				</div>
				<div class="titleBig">
					<a href="#" target="_parent" class="lianhui" onclick="return false">
						<img src="<c:url value="/images/portal/proCase/lianhui.png"/>"
						alt="lianhui" />
						<div class="title">联汇通宝</div>
					</a>
				</div>
			</div>
		</div>
		<iframe src="success/phoneQQ" id="sucIframe" name="sucIframe"
			scrolling="no" style="width: 100%; height: 830px; border: none;"></iframe>
	</div>

	<div class="footer">
		<div class="Copyright">Copyright 版权所有：成都卡卡付科技有限公司 All Right
			Reserve 蜀ICP备16003612号</div>
	</div>
	<script>
		$('.logo a')
				.on(
						'click ',
						function() {
							if ($(this).parent().hasClass("active") == false) {
								$(this).parent().addClass("active");
								$(this).parent().siblings().removeClass(
										"active");
							}
							var imgSrc = $(this).children("img").attr("src");
							var newImgSrc = imgSrc.split(".png")[0];
							var startNum = newImgSrc.lastIndexOf("/");
							var srcVal = newImgSrc.substr(startNum + 1);
							if (srcVal.indexOf("N") == -1) {
								$(this)
										.children("img")
										.attr("src",
												"<c:url value='/images/portal/proCase/"+srcVal+"N.png '/>");
								var paImg = $(this).parent().siblings()
										.children("a").children("img");
								$(paImg)
										.each(
												function() {
													var otherSrcVal = $(this)
															.attr("alt");
													$(this)
															.attr("src",
																	"<c:url value='/images/portal/proCase/"+otherSrcVal+".png '/>");
												});

							}

							var aVal = $(this).attr("class");
							$('#sucIframe').attr("src", "success/" + aVal);

						});

		if (isFirefox = navigator.userAgent.indexOf("Firefox") > 0) {
			$('li img').css({
				"margin-bottom" : "-8px"
			});
		}
	</script>
</body>
</html>

