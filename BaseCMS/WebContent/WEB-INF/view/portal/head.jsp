<%@ page pageEncoding="UTF-8"%>
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<meta name="renderer" content="webkit" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="keywords" content="微信支付" />
<meta name="description" content="微信支付" />
<link rel="stylesheet" href="<c:url value="/css/bootstrap/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/js/fullPage/css/jquery.fullPage.css"/>" />
<!--[if lt IE 9]>
  <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="<c:url value="/css/portal/index.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/portal/common.css"/>" />
<script src="<c:url value="/js/JQuery/jquery-1.10.0.min.js"/>"></script>
<script src="<c:url value="/css/bootstrap/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/js/JQuery/jquery-ui-1.10.4.min.js"/>"></script>
<script src="<c:url value="/js/fullPage/js/jquery.fullPage.min.js"/>"></script>
<script>
	var heightWin = $(window).height();
	function imgAuto(objImg) {
		if(heightWin <800) {
			var heightNew = ($(objImg).height()) * 0.6;
			$(objImg).height(heightNew);
		}
	}
</script>

<link rel="stylesheet" type="text/css"
	href='<c:url value="/hplus/css/bootstrap.min14ed.css?v=3.3.6"/>'>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/hplus/css/font-awesome.min93e3.css?v=4.4.0"/>'>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/hplus/css/animate.min.css"/>'>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/hplus/css/style.min862f.css?v=4.1.0"/>'>
