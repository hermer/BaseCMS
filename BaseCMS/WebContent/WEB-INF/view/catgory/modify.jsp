<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
<style>
.table-bordered {
	text-align: left;
}

.table-bordered>tbody tr:nth-child(odd) {
	background: #fff;
}

.table-bordered>tbody tr td {
	padding-left: 40px;
	line-height: 30px;
}

.preTitle {
	width: 80px;
	display: -moz-inline-box;
	display: inline-block;
}

.upImg {
	margin-left: 95px;
	margin-top: 10px;
}

.item {
	margin: 15px;
}
</style>
</head>
<body>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form class="form-inline form-index" id="form">
				<div id="uploadImg" class="item">
					<font color="red">*</font><span class="preTitle">栏目名称:</span> <input
						name="name" id="name" type="text" value="${entity.name }"
						class="form-control" />
				</div>
				<div id="uploadImg" class="item">
					<font color="red">*</font><span class="preTitle">公众号:</span> <input
						name="wxAccount" id="wxAccount" type="text" value="${entity.wxAccount }"
						class="form-control" />
				</div>
                <div class="item">
					<font color="red">*</font><span class="preTitle">栏目状态:</span>
					<html:select cssClass="form-control" collection="catgoryStatus"
						selectValue="${entity.status }" name="status" id="status">
					</html:select>
				</div>
				<div class="item">
					<font color="white">*</font> <span class="preTitle">显示顺序:</span> <input
						name="showOrder" id="showOrder" value="${entity.showOrder }"
						type="text" class="form-control" />
				</div>
				<div class="item">
					<font color="white">*</font> <span class="preTitle">栏目描述:</span> <input
						name="description" id="description" type="text"
						value="${entity.description }" class="form-control" />
				</div>
				<input type="hidden" name="id" value="${entity.id }"> <input
					type="button" value="保存" class="btn btn-default"
					onclick="doSubmit();">
			</form>
		</div>
	</div>
	<script type="text/javascript">
		function doSubmit() {
			if ($("#name").val() == "") {
				layer.alert("栏目不能为空");
				return;
			}
			if($("#wxAccount").val()==""){
				layer.alert("公众号不能为空");
				return;
			}
			if ($("#showOrder").val() == "") {
				layer.alert("显示顺序不能为空");
				return;
			}
			var data = $("#form").serialize();
			$.ajax({
				url : "modify",
				data : data,
				type : 'post',
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.success) {
						layer.alert('操作成功', {
							icon : 6
						},
								function() {
									var index = parent.layer
											.getFrameIndex(window.name); //获取窗口索引
									parent.layer.close(index);
								});
						parent.reload();
					} else {
						layer.alert('操作失败:' + result.obj);
					}
				},
				error : function() {
					layer.alert('操作失败:系统异常');
				}
			});
		}
	</script>
</body>
</html>