<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<div class="table-container" id="content">
	<table>
		<thead>
			<tr>
				<th align="center" width="200px">出发地点</th>
				<th align="center" width="200px">目的地点</th>
				<th align="center" width="200px">出发日期</th>
				<th align="center" width="100px">客单价</th>
				<th align="center" width="100px">座位数</th>
				<th align="center" width="100px">状态</th>
				<th align="center" width="100px">备注</th>
				<th align="center" width="140px">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${list}" var="item">
				<tr>
					<td align="center">${item.fromAddr}</td>
					<td align="center">${item.toAddr}</td>
					<td align="center">${item.goDate}</td>
					<td align="center">${item.price}</td>
					<td align="center">${item.seats}</td>
					<td align="center">${item.statusName}</td>
					<td align="center">${item.remark}</td>
					<td align="center">
					    <html:auth res="/bus/edit">
							<a target="_self" href="bus/edit?id=${item.id}">修改</a>
						</html:auth>
						 <html:auth res="/bus/view">
							<a target="_self" href="bus/view?id=${item.id}">详情</a>
						</html:auth>
						 <html:auth res="/bus/del">
							<a target="_self" href="#" onclick="isDel('${item.id}')">删除</a>
						</html:auth>
						</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div id="kkpager"></div>
</div>
<script type="text/javascript">
$(function() {
	showPager('${pageNo}','${pageSize}','${totalCount}');
})
</script>
</html>