/*
 * jQuery Reveal Plugin 1.0
 * Free to use under the MIT license.
*/


(function($) {
/*---------------------------
 Defaults for Reveal
----------------------------*/
/*---------------------------
 Listener for data-reveal-id attributes
----------------------------*/
	//$('a[data-reveal-id]').live('click', function(e) {
	$('[data-reveal-id]').live('click', function(e) {
		e.preventDefault();
		var modalLocation = $(this).attr('data-reveal-id');
		//在top页面获取mainFrame中的元素
		var mainFrame=window.parent.document.getElementById('mainFrame');
				if (mainFrame != null) {
					$(
							mainFrame.contentWindow.document.getElementById(modalLocation)).reveal(
							$(this).data());
				} else {
					// 点出处与弹出处在同一页面
					var id = $(this).attr('load-id');
					var url = $(this).attr('load-url');
					if (url == "datadict/modify") {
						loadData(url, id);
					}else if (modalLocation == "addTerminal") {
						getTerminal(url);
					}
					$('#' + modalLocation).reveal($(this).data());
				}
			});
	/*
	 * 加载数据字典
	 */
function loadData(url,id){
	$.ajax({
		url:url,
		type:"GET",
		data:{
			id:id,
		},
		success:function(data){
			$("#modify_dict").find("#name").attr("value",data.name).attr("checked","checked");
			$("#modify_dict").find("#value").attr("value",data.value);
			$("#modify_dict").find("#description").attr("value",data.description);
			$("#modify_dict").find("#id").attr("value",data.id);
			$("#modify_dict").find("#nameCN").attr("value",data.nameCN);
		}
	});
}
/*
 * 加载目标银行终端
 */
	function getTerminal(url) {
		$.ajax({
             url:url,
             type:'post',
             data : {
 				terminalId : $('#terminalId').val(),
 				tradeChannelCode:$("#tradeChannelCode").val(),
 				merchantNumber:$("#merchantNumber").val(),
 				terminalNumber:$("#terminalNumber").val(),
 			},
             success:function(html){
             $("#model_content").replaceWith(html);
             }
		});
	}
/*---------------------------
 Extend and Execute
----------------------------*/

    $.fn.reveal = function(options) {
        var defaults = {  
	    	animation: 'fadeAndPop', //fade, fadeAndPop, none
		    animationspeed: 300, //how fast animtions are
		    closeonbackgroundclick: true, //if you click background will modal close?
		    dismissmodalclass: 'close-reveal-modal' //the class of a button or element that will close an open modal
    	}; 
    	
        //Extend dem' options
        var options = $.extend({}, defaults, options); 
        return this.each(function() {
/*---------------------------
 Global Variables
----------------------------*/
        	var modal = $(this),
        		topMeasure  = parseInt(modal.css('top')),
				topOffset = modal.height() + topMeasure,
          		locked = false,
				modalBG = $('.reveal-modal-bg');

/*---------------------------
 Create Modal BG
----------------------------*/
			if(modalBG.length == 0) {
				modalBG = $('<div class="reveal-modal-bg" id="myReveal" />').insertAfter(modal);
			}		    
     
/*---------------------------
 Open & Close Animations
----------------------------*/
			//Entrance Animations
			modal.bind('reveal:open', function () {
			  modalBG.unbind('click.modalEvent');
				$('.' + options.dismissmodalclass).unbind('click.modalEvent');
				if(!locked) {
					lockModal();
					if(options.animation == "fadeAndPop") {
						modal.css({'top': 100,'display':'',  'opacity' : 0, 'visibility' : 'visible'});
						//modal.css({'top': $(document).scrollTop()-topOffset,'display':'',  'opacity' : 0, 'visibility' : 'visible'});
						modalBG.fadeIn(options.animationspeed/2);
						modal.delay(options.animationspeed/2).animate({
							"top": $(document).scrollTop()+topMeasure + 'px',
							"opacity" : 1
						}, options.animationspeed,unlockModal());					
					}
					if(options.animation == "fade") {
						modal.css({'opacity' : 0,'visibility' : 'visible', 'top': $(document).scrollTop()+topMeasure});
						modalBG.fadeIn(options.animationspeed/2);
						modal.delay(options.animationspeed/2).animate({
							"opacity" : 1
						}, options.animationspeed,unlockModal());					
					} 
					if(options.animation == "none") {
						modal.css({'visibility' : 'visible', 'top':$(document).scrollTop()+topMeasure});
						modalBG.css({"display":"block"});	
						unlockModal()				
					}
				}
				modal.unbind('reveal:open');
			}); 	

			//Closing Animation
			modal.bind('reveal:close', function () {
			  if(!locked) {
					lockModal();
					if(options.animation == "fadeAndPop") {
						modalBG.delay(options.animationspeed).fadeOut(options.animationspeed);
						modal.animate({
							"top":  $(document).scrollTop()-topOffset + 'px',
							"opacity" : 0
						}, options.animationspeed/2, function() {
							modal.css({'top':topMeasure, 'opacity' : 1, 'visibility' : 'hidden'});
							unlockModal();
						});					
					}  	
					if(options.animation == "fade") {
						modalBG.delay(options.animationspeed).fadeOut(options.animationspeed);
						modal.animate({
							"opacity" : 0
						}, options.animationspeed, function() {
							modal.css({'opacity' : 1, 'visibility' : 'hidden', 'top' : topMeasure});
							unlockModal();
						});					
					}  	
					if(options.animation == "none") {
						modal.css({'visibility' : 'hidden', 'top' : topMeasure});
						modalBG.css({'display' : 'none'});	
					}		
				}
				modal.unbind('reveal:close');
				//关闭弹窗刷新当前页面
				var mainFrame=window.parent.document.getElementById('mainFrame');
				if(mainFrame!=null){
				window.parent.location.reload();
				}
			});     
   	
/*---------------------------
 Open and add Closing Listeners
----------------------------*/
        	//Open Modal Immediately
    	modal.trigger('reveal:open');
			
			//Close Modal Listeners
			var closeButton = $('.' + options.dismissmodalclass).bind('click.modalEvent', function () {
			  modal.trigger('reveal:close');
			});
			
			if(options.closeonbackgroundclick) {
				modalBG.css({"cursor":"pointer"});
				modalBG.bind('click.modalEvent', function () {
				modal.trigger('reveal:close');
				});
			}
			$('body').keyup(function(e) {
        		if(e.which===27){ modal.trigger('reveal:close'); } // 27 is the keycode for the Escape key
			});
			
/*---------------------------
 Animations Locks
----------------------------*/
			function unlockModal() { 
				locked = false;
			}
			function lockModal() {
				locked = true;
			}	
			
        });//each call
    }//orbit plugin call
})(jQuery);
        
